// Object definition
const greeter = {
    // Method hello takes a parameter name and prints to console
    hello: function(name) {
        console.log("Hello " + name);
    }
};

// Example usage of the hello method
greeter.hello("John"); // Prints "Hello John" to the console with the command node .\problem1a.js
