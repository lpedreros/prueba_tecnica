function ConteodeCiudades(citiesList) {
    //como primer paso se crea un objeto en donde se almacenara la cantidad de veces que aparece una ciudad 
    const conteociudad = {};
    citiesList.forEach(ciudad => {
        conteociudad[ciudad] = (conteociudad[ciudad] || 0) + 1;
    });
    //el forEach recorre todo el array de citiesList y le suma 1 cada vez que ve que la ciudad en analice vuelve a aparecer.

    
    const countsArray = Object.entries(conteociudad);

    //Ahora se realiza un cambio de objeto a array y ese array presenta varios arrays.

    // Ordena la matriz según el recuento en orden descendente
    countsArray.sort((a, b) => b[1] - a[1]);

    // Registra las 5 ciudades principales
    console.log("las 5 ciudad con mayor frecuencia:");
    for (let i = 0; i < Math.min(5, countsArray.length); i++) {
        console.log(`${i + 1}. ${countsArray[i][0]}: ${countsArray[i][1]} de frecuencia`);
    }
}

const citiesList = [
    "nasville",
    "nasville",
    "los angeles",
    "nasville",
    "Madrid",
    "memphis",
    "barcelona",
    "los angeles",
    "sevilla",
    "Madrid",
    "canary islands",
    "barcelona",
    "Madrid",
    "Madrid",
    "nasville",
    "barcelona",
    "london",
    "berlin",
    "Madrid",
    "nasville",
    "london",
    "Madrid",
    "Madrid",
];

ConteodeCiudades(citiesList);