const greeter = {
    hello: function(name) {
        // Hacer una copia del nombre para mantenerlo inmutable
        const immutableName = Object.freeze(name);
        console.log("Hello " + immutableName);
    }
};

greeter.hello("John"); // Prints "Hello John" to the console with the command node .\problem1b.js

/*The greeter object contains a method called hello which accepts a parameter name.
Inside the hello method, a copy of the name parameter is created using Object.freeze() to make it immutable. This prevents any modifications to the name object.
The method then prints a greeting message to the console using the immutable name.
Finally, the hello method is called with the name "John".*/